<?php
/**
 * Plugin Name: Social Justice Share Buttons
 * Description: Provides Social Buttons sharing blocks for post a custom link and message for sharing.
 *
 * @since  1.0
 *
 * @package social-justice-media-blocks
 * Version: 1.0
 * Text Domain: socialjustice
 **/

// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once 'vendor/autoload.php';

/**
 * Renders all the ACF fields used bu the plugin.
 */
require_once 'inc/acf.php';
/**
 * Contains the code that defines the ACF Blocks.
 */
require_once 'inc/acf-blocks.php';

/**
	* Loads the scripts and stylesheets used by the plugin
	*/
require_once 'inc/plugin-assets.php';

