<?php
/**
 * Contains the definition of elements that will be loaded to offer plugin functionality.
 * These elements are scripts and stylesheets.
 *
 * @package social-justice-media-blocks
 *
 */

/**
 * Loads the styles that modify the look and feel of the plugin's interface elements.
 *
 * @return void
 */
function load_custom_styles() {
	wp_enqueue_style( 'share_buttons_styles', plugin_dir_url( __DIR__ ) . 'assets/css/share-buttons-style.css', false, '1.0');
}
add_action( 'wp_enqueue_scripts', 'load_custom_styles' );