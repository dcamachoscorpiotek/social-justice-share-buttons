<?php
/**
 * Contains the functions that will register the block into WordPress.
 *
 * @since 1.0
 *
 * @package social-justice-media-blocks
 */

/**
 * Register_register_socialjustice_category_block Creates the 'social-share-buttons-blocks' category.
 * Later on when the actual block is created, we specify the 'social-share-buttons-blocks'
 * category created below for the block to live under.
 *
 * @param  array $categories the current block categories.
 * @param  array $post
 *
 * @return void
 */
function register_socialjustice_category_block( $categories) {
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'social-share-buttons-blocks',
				'title' => __( 'Social Share Buttons', 'socialjustice' ),
				'icon'  => 'dashicons-welcome-learn-more',
			),
		)
	);
}
add_filter( 'block_categories', 'register_socialjustice_category_block', 10, 2 );

/**
 * Register_social_media_buttons registers the actual social media button block with WordPress.
 *
 * @return void
 */
function register_share_buttons() {
	acf_register_block(
		array(
			'name'            => 'social_justice_share_buttons',
			'title'           => __( 'Share Buttons', 'socialjustice' ),
			'description'     => __( 'Social buttons for share custom content', 'socialjustice' ),
			'render_template' => plugin_dir_path( __FILE__ ) . 'partials/share-buttons.php',
			'category'        => 'social-share-buttons-blocks',
			'keywords'        => [ 'button', 'share', 'social' ],
			'mode'            => 'edit',
			'icon'            => array(
				'background' => '#ffffff',
				'src'        => 'share',
			),
		)
	);
}
add_action( 'acf/init', 'register_share_buttons' );