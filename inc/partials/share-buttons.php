<?php
/**
	* Template that defines how the blocks are going to be displayed. We fetch the values from
	* the ACF fields the user defined in runtime and display their content here.
	*
	* @since 1.0
	*
	* @package social-justice-media-blocks
	*/

//Facebook button
$facebook_message = str_replace('"',"'",get_field('facebook_message'));
$facebook_link_obj = get_field('facebook_link');
$facebook_link = esc_url($facebook_link_obj['url']);
$link_facebook_target = '_self';

//Twitter button
$twitter_message = str_replace('"',"'",get_field('twitter_message'));
$twitter_link_obj = get_field('twitter_link');
$twitter_link = esc_url($twitter_link_obj['url']);
$link_twitter_target = '_self';

/** We only change the target attribute if what was fetched for the link is an array object
	* and if the index of 'target' actually exists we set the target as such. Otherwise we just
	* leave the target to _self.
	*/
if (is_array($facebook_link_obj) && array_key_exists('target', $facebook_link_obj)) {
	$link_facebook_target = $facebook_link_obj['target'];
}

if (is_array($twitter_link_obj) && array_key_exists('target', $twitter_link_obj)) {
	$link_twitter_target = $twitter_link_obj['target'];
}
?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : '726184257853401', // you need to create an facebook app
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.3'
        });
    };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<!-- load jquery -->
<a href="#" class="share-btn" id="share-btn" target="<?php echo esc_html($link_facebook_target); ?>">
	<div class="facebook-btn" style="display:inline-block">
		Share on Facebook
	</div>
</a>

<a class="share-btn" id="share-twitter" onclick="share_twitter()" href="#" target="<?php echo esc_html($link_twitter_target); ?>">
	<div class="twitter-btn" style="display:inline-block">
		Share on Twitter
	</div>
</a>

<script>
    jQuery('#share-btn').on('click', function () {
        FB.ui({
            method: "share",
            href: "<?php echo esc_url($facebook_link); ?>",
            quote: "<?php echo $facebook_message; ?>",
        }, function (response) { });
    });

    function share_twitter() {
        window.open("https://twitter.com/share?text=<?php echo $twitter_message; ?>&url=<?php echo esc_url($twitter_link); ?>", "twitter share", "width=500,height=360,resizable=no,toolbar=no,menubar=no,location=no,status=no");
        return false;
    }
</script>
