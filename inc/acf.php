<?php
/**
	* Contains the definition of all the ACF fields used by the Social Justice Network.
	*
	* @since 1.0
	*
	* @package social-justice-media-blocks
	*/

use \StoutLogic\AcfBuilder\FieldsBuilder;

if (class_exists(FieldsBuilder::class)) {
	add_action('after_setup_theme', 'create_share_buttons_fields');
}

/**
	* Creates the fields that will be used in the template to render choices that the
	* user will have, such as specifying the link of the button and the message that the
	* button should share.
	*
	* @return void
	*/
function create_share_buttons_fields()
{
	$social_share_button_fields = new FieldsBuilder('social_share_button');
	$social_share_button_fields
			->addTextarea(
					'facebook_message',
					array(
							'label' => 'Facebook Message',
							'instructions' => 'The message to be displayed on facebook share pop up.',
							'default_value' => '',
							'required' => 1,
							'wrapper' => array(
									'width' => 50,
							),
					)
			)
			->addLink(
					'facebook_link',
					array(
							'label' => 'Custom Facebook Share Link',
							'instructions' => 'The link to be shared in the facebook share pop-up',
							'return_format' => 'array',
							'required' => 1,
							'wrapper' => array(
									'width' => 50,
							),
					)
			)
			->addTextarea(
					'twitter_message',
					array(
							'label' => 'Twitter Message',
							'instructions' => 'The message to be displayed on twitter share pop up.',
							'default_value' => '',
							'required' => 1,
							'wrapper' => array(
									'width' => 50,
							),
					)
			)
			->addLink(
					'twitter_link',
					array(
							'label' => 'Custom Twitter Share Link',
							'instructions' => 'The link to be shared in the twitter share pop-up',
							'return_format' => 'array',
							'required' => 1,
							'wrapper' => array(
									'width' => 50,
							),
					)
			)
			/** This has to match the type that is defined inside acf-blocks. For some obscure reason
				* this has to be defined using dashes even though the block name is defined using underscores.
				* I'll buy you a beer if you can find the reasoning behind this.
				*/
			->setLocation('block', '==', 'acf/social-justice-share-buttons');

	add_action(
			'acf/init',
			function () use ($social_share_button_fields) {
				acf_add_local_field_group($social_share_button_fields->build());
			}
	);
}
